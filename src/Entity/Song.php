<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Validator\Constraints\Time;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SongRepository")
 */
class Song
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $singer;

    /**
     * @ORM\Column(type="date")
     */
    private $duration;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $picturePath;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPlayed;

    /**
     * @ORM\Column(type="integer")
     */
    private $likeCounter;

    /**
     * @ORM\Column(type="date")
     */
    private $parution;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSinger(): ?string
    {
        return $this->singer;
    }

    public function setSinger(string $singer): self
    {
        $this->singer = $singer;

        return $this;
    }

    public function getDuration(): ?DateTime
    {
        return $this->duration;
    }

    public function setDuration(DateTime $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getPicturePath(): ?string
    {
        return $this->picturePath;
    }

    public function setPicturePath(string $picturePath): self
    {
        $this->picturePath = $picturePath;

        return $this;
    }

    public function getIsPlayed(): ?bool
    {
        return $this->isPlayed;
    }

    public function setIsPlayed(bool $isPlayed): self
    {
        $this->isPlayed = $isPlayed;

        return $this;
    }

    public function getLikeCounter(): ?int
    {
        return $this->likeCounter;
    }

    public function setLikeCounter(int $likeCounter): self
    {
        $this->likeCounter = $likeCounter;

        return $this;
    }

    public function getParution(): ?\DateTimeInterface
    {
        return $this->parution;
    }

    public function setParution(\DateTimeInterface $parution): self
    {
        $this->parution = $parution;

        return $this;
    }
}
