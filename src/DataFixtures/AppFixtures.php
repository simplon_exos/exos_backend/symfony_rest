<?php

namespace App\DataFixtures;

use App\Entity\Hero;
use App\Entity\Power;
use App\Entity\Song;
use App\Entity\User;
use DateTimeInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\Time;


class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)
    {
        // on instancie faker pour générer des données aléatoires
        $faker = Factory::create();

        // POWERS
        // On déclare un array vide dans lequel on va stocker les pouvoirs générés
        $powers = [];
        for ($i = 0; $i < 5; $i++) {
            $power = new Power();
            $power->setName('power ' . $i)
                ->setType('type ' . $i);
            $manager->persist($power);
            $powers[] = $power;
        }

        // HEROES
        for ($i = 0; $i < 5; $i++) {
            $hero = new Hero();
            $hero->setName($faker->name)
                ->setLevel($faker->randomNumber())
                ->setBirth($faker->dateTime())
                ->setTitle('The Great ' . $faker->firstName)
                ->addPower($powers[rand(0, 4)]);
            $manager->persist($hero);
        }

        // USERS
        for ($i = 0; $i < 3; $i++) {
            $user = new User();
            $hashedPassword = '$argon2id$v=19$m=65536,t=4,p=1$yZVOWpQR9n+JW/GQ4osJfQ$YnwLKg9+bk/IXdx+pGijgzOOzCizCELjt6ZNKaLKoGI';
            $user->setEmail($faker->email())
                ->setRoles(['ROLE_USER'])
                ->setPassword($hashedPassword);
            $manager->persist($user);
        }
        $user = new User();
        $hashedPassword = $this->encoder->encodePassword($user, '1234');
        $user->setEmail('angular@mail.com')
            ->setRoles(['ROLE_USER'])
            ->setPassword($hashedPassword);
        $manager->persist($user);


        // SONGS
        for ($i = 0; $i < 11; $i++) {
            $song = new Song();
            $song->setTitle($faker->realText($maxNbChars = 25))
                ->setSinger($faker->name())
                ->setPicturePath($faker->imageUrl())
                ->setParution($faker->dateTime())
                ->setDuration($faker->dateTime())
                ->setIsPlayed(false)
                ->setLikeCounter($faker->randomDigitNotNull());
            $manager->persist($song);
        }


        $manager->flush();
    }
}
