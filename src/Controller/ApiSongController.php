<?php

namespace App\Controller;

use App\Entity\Song;
use App\Form\SongType;
use App\Repository\SongRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/song", name="api_song")
 */
class ApiSongController extends AbstractController
{
    /**
     * @Route(methods="GET")
     */
    public function index(SongRepository $repo)
    {
        return $this->json($repo->findAll());
    }

    /**
     * @Route("/{id}", methods="GET")
     */
    public function one(Song $song)
    {
        return $this->json($song);
    }

    /**
     * @Route("/{id}", methods="PATCH")
     * @Route(methods="POST")
     */
    public function add(Song $song = null, Request $request, ObjectManager $manager){
        if(!$song) {
            $song = new Song();
        }
        $form = $this->createForm(SongType::class, $song);
        $form->submit(
            json_decode(
                $request->getContent(),
                true
                ),
                false
            );

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($song);
            $manager->flush();
            return $this->json($song, 201);
        }
        return $this->json($form->getErrors(true), 400);

    }
}
