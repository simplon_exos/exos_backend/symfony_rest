<?php

namespace App\Controller;

use App\Entity\Hero;
use App\Form\HeroType;
use App\Repository\HeroRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/hero", name="api_hero")
 */
class ApiHeroController extends AbstractController
{

    /**
     * @Route(methods="GET")
     */
    public function index(HeroRepository $repo)
    {
        // on renvoie une réponse HTTP dans laquelle Symfony va convertir
        // en JSON les instances qu'on lui passe en argument (Tous les objets du repo)
        return $this->json($repo->findAll());
    }


    /**
     * @Route("/{id}", methods="GET")
     */
    public function one(Hero $hero)
    {
        return $this->json($hero);
    }


    /**
     * @Route("/{id}", methods="DELETE")
     */
    public function delete(Hero $hero, ObjectManager $manager)
    {
        $manager->remove($hero);
        $manager->flush();

        return $this->json(null, 204);
    }


    /* On peut mettre 2 routes sur une même méthode (modifier et ajouter)
     * Comme l'une d'elle a un paramètre de route, il faut mettre une valeur 
     * par défaut à null pour ce paramètre dans la méthode (pour que ça fonctionne
     * aussi pour le POST)
     */

    /**
     * @Route("/{id}", methods="PATCH") 
     * @Route(methods="POST")
     */
    public function add(Hero $hero = null, Request $request, ObjectManager $manager)
    {
        // Comme on a 2 méthodes qui pointent sur cette route, on vérifie 
        // tout d'abord si l'instance existe (POST ou PATCH)
        if (!$hero) {
            $hero = new Hero();
        }
        $form = $this->createForm(HeroType::class, $hero);

        // On ne fait pas handleRequest mais on submit manuellement
        // La méthode submit() attend un tabeau associatif contenant
        // les clés et valeur du form
        $form->submit(
            // On utilise la méthode json_decode pour transformer le contenu
            // de la request (qui est une chaine de caractères json) en 
            // tableau associatif
            json_decode(
                $request->getContent(),
                true
                ),
                false
            );

        // puis on vérifie toujours la validation du form
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($hero);
            $manager->flush();
            // On renvoie un 201 pour "success created"
            return $this->json($hero, 201);
        }
        // Sinon on renvoie un 400 "bad request" avec les erreurs du form
        return $this->json($form->getErrors(true), 400);
    }
}
